from datetime import datetime


def stringToDate(date):
    return datetime.strptime(date, '%Y-%m-%d').date()


def getIntervalDate(start_date, end_date):
    start_date = stringToDate(start_date)
    end_date = stringToDate(end_date)
    diff = end_date - start_date
    return diff.days
