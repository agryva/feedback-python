from flask import Flask, jsonify, request
from sqlalchemy import text
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgre@localhost/jds'
db = SQLAlchemy(app)
api = Api(app)


@app.route('/get-all')
def get():
    results = db.session.execute(text('select * from feedback'))
    response = []
    for value in results:
        response.append({
            'id': value.id,
            'date': value.date,
            'category': value.category,
            'score': value.score
        })
    return jsonify({
        'count': len(response),
        'data': response
    })


@app.route('/presentase')
def getPresentase():
    args = request.args
    start_date = args.get('start_date')
    end_date = args.get('end_date')
    query = '''
        select sum(fb.score) as jumlah, fb.category, 
            (sum(fb.score) / (count(*) * 4)::float * 100) as presentase,
            count(*) as total
            from feedback AS fb
            group by fb.category
    '''

    if end_date is not None and start_date is not None:
        query = f'''SELECT SUM(fb.score) as jumlah, fb.category,
            (SUM(fb.score) / (COUNT(*) * 4)::float * 100) as presentase,
            COUNT(*) as total
        FROM (
        SELECT score, category FROM feedback WHERE date BETWEEN  '{start_date}' and '{end_date}'
        ) AS fb
        GROUP BY fb.category;
'''

    results = db.session.execute(text(query))
    response = []

    for value in results:
        response.append({
            'category': value[1],

            'jumlah_score': int(value[0]),
            'presentase': value[2],
            'total': value[3]
        })
    return jsonify({
        'data': response
    })


@app.route('/average')
def getAverage():
    args = request.args
    month = args.get('month')
    query = '''
        select sum(fb.score) as jumlah, fb.category, 
        avg(fb.score) as average
        from feedback AS fb
        group by fb.category
    '''

    if month is not None:
        query = f'''select sum(fb.score) as jumlah, fb.category, 
            avg(fb.score) as average
            from feedback as fb
            where date_trunc('month', date) = date_trunc('month', to_date('{month}', 'yyyy-mm'))
            group by fb.category'''

    results = db.session.execute(text(query))
    response = []

    for value in results:
        response.append({
            'category': value[1],
            'jumlah_score': int(value[0]),
            'rata-rata': float(value[2])
        })
    return jsonify({
        'data': response
    })


@app.route('/sum')
def getSum():
    args = request.args
    start_date = args.get('start_date')
    end_date = args.get('end_date')
    type = args.get('type')

    if end_date is None or start_date is None or type is None:
        return jsonify({
            'message': 'start date, end date & type required'
        })

    if type == 'daily':
        where = "date_trunc('day', date)"
    elif type == 'weekly':
        where = "date_trunc('week', date)"
    elif type == 'monthly':
        where = "date_trunc('month', date)"
    elif type == 'quartal':
        where = "date_trunc('quarter', date)"
    else:
        where = "date"

    query = f'''
        WITH dr AS (
        SELECT
            '{start_date}'::date AS start_date,
            '{end_date}'::date AS end_date
        ),
        dc AS (
        SELECT
            category,
            date_trunc('day', date) AS date,
            sum(score) AS count,
            count(*) as total
        FROM
            feedback
        WHERE
            date BETWEEN (SELECT start_date FROM dr) AND (SELECT end_date FROM dr)
        GROUP BY
            category,
            date_trunc('day', date)
        )
        SELECT {where} as tgl, category, SUM(count) as total_score, SUM(total) as total_data
        FROM dc
        GROUP BY {where}, category

    '''

    results = db.session.execute(text(query))
    response = []

    for value in results:
        response.append({
            'category': value[0],
            'tanggal': str(value[1]),
            'jumlah_score': int(value[2]),
            'presentase': "{0:,.2f}".format(int(value[2]) / (int(value[3]) * 4) * 100),
            'total': int(value[3]),
        })
    return jsonify({
        'data': response
    })


@app.route('/diff-presentasse')
def getDiffPresentase():
    queryCurrentMonth = '''
        select sum(fb.score) as jumlah, fb.category, 
        (count(fb.score)) as total 
        from feedback as fb
        where extract(YEAR FROM date) = extract(YEAR FROM now()) and extract(MONTH FROM date) = extract(MONTH FROM now()) 
        group by fb.category
    '''
    queryLastMonth = '''
        select sum(fb.score) as jumlah, fb.category, 
        (count(fb.score)) as total 
        from feedback as fb
        where extract(YEAR FROM date) = extract(YEAR FROM now()) and extract(MONTH FROM date) = extract(MONTH FROM now() -interval '1 month') 
        group by fb.category
    '''

    resultsCurrentMonth = db.session.execute(text(queryCurrentMonth))
    resultsLastMonth = db.session.execute(text(queryLastMonth))

    responseCurrentMonth = []
    responseLastMonth = []
    response = []

    for value in resultsCurrentMonth:
        responseCurrentMonth.append({
            'category': value[1],
            'jumlah_score': int(value[0]),
            'total': int(value[2])
        })

    for value in resultsLastMonth:
        responseLastMonth.append({
            'category': value[1],
            'jumlah_score': int(value[0]),
            'total': int(value[2])
        })

    for obj1 in responseCurrentMonth:
        for obj2 in responseLastMonth:
            if obj1['category'] == obj2['category']:
                obj1['precentage'] = (
                    (obj1["jumlah_score"] - obj2["jumlah_score"]) / obj2["jumlah_score"]) * 100
                break
            else:
                obj1['precentage'] = 0
        response.append(obj1)

    for obj2 in responseLastMonth:
        if obj2 not in responseCurrentMonth:
            obj2['precentage'] = 0
            response.append(obj2)

    return jsonify({
        'data': response
    })


# @app.route('/percentage')
# def getPercentage():
#     args = request.args
#     start_date = args.get('start_date')
#     end_date = args.get('end_date')
#     type = args.get('type')

#     if end_date is None or start_date is None or type is None:
#         return jsonify({
#             'message': 'start date, end date & type required'
#         })

#     if type == 'daily':
#         where = "date_trunc('day', date)"
#     elif type == 'weekly':
#         where = "date_trunc('week', date)"
#     elif type == 'monthly':
#         where = "date_trunc('month', date)"
#     elif type == 'quartal':
#         where = "date_trunc('quarter', date)"
#     else:
#         where = "date"

#     query = f'''
#         WITH date_range AS (
#         SELECT
#             '{start_date}'::date AS start_date,
#             '{end_date}'::date AS end_date
#         ),
#         category_data AS (
#         SELECT
#             category,
#             date_trunc('day', date) AS date,
#             sum(score) AS count,
#             count(*) as data_count
#         FROM
#             feedback
#         WHERE
#             date BETWEEN (SELECT start_date FROM date_range) AND (SELECT end_date FROM date_range)
#         GROUP BY
#             category,
#             date_trunc('day', date)
#         )
#         SELECT
#             category,
#             {where} AS time_period,
#             sum(count) AS total_count,
#             count(data_count) AS acount
#         FROM
#             category_data
#         GROUP BY
#             category,
#             time_period
#         ORDER BY
#             category,
#             time_period;

#     '''

#     results = db.session.execute(text(query))
#     response = []

#     for value in results:
#         response.append({
#             'category': value[0],
#             'tanggal': str(value[1]),
#             'jumlah_score': int(value[2]),
#             'total': int(value[3]),
#         })
#     return jsonify({
#         'data': response
#     })


if __name__ == '__main__':
    app.run(debug=True)
