from flask import Flask, jsonify, request
import os
from sqlalchemy import text
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from dotenv import load_dotenv
from helpers import getIntervalDate
load_dotenv()

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DB_URI')
db = SQLAlchemy(app)
api = Api(app)


@app.route('/percentage')
def getPercentageOne():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')

        data = []
        response = {}

        query = '''
        select
            sum(fb.score) as total_score, count(*) as total_data,
            (SUM(fb.score) / (COUNT(*) * 4)::float * 100) as percentage
        from feedback as fb
        '''

        if end_date is not None and start_date is not None:
            query = f'''
            select
                sum(fb.score) as total_score, count(*) as total_data,
                (SUM(fb.score) / (COUNT(*) * 4)::float * 100) as percentage
            from feedback as fb
            WHERE date BETWEEN  '{start_date}' and '{end_date}'
            '''

        results = db.session.execute(text(query))

        if end_date is not None and start_date is not None:
            response['interval'] = f'{getIntervalDate(start_date, end_date)} Days'

        for value in results:
            data.append({
                'total_score': int(value[0]),
                'total_data': int(value[1]),
                'percentage': float(value[2])
            })

        response['data'] = data[0]

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500


@app.route('/percentage-list')
def getPercentageList():
    args = request.args
    start_date = args.get('start_date')
    end_date = args.get('end_date')

    data = []
    response = {}

    query = '''
        select sum(fb.score) as total_score, fb.category,
            (sum(fb.score) / (count(*) * 4)::float * 100) as percentage,
            count(*) as total
            from feedback AS fb
            group by fb.category
    '''

    if end_date is not None and start_date is not None:
        query = f'''SELECT SUM(fb.score) as total_score, fb.category,
            (SUM(fb.score) / (COUNT(*) * 4)::float * 100) as percentage,
            COUNT(*) as total
            FROM (
            SELECT score, category FROM feedback WHERE date BETWEEN  '{start_date}' and '{end_date}'
            ) AS fb
            GROUP BY fb.category;
        '''

    results = db.session.execute(text(query))

    for value in results:
        data.append({
            'total_score': int(value[0]),
            'category': value[1],
            'percentage': float(value[2]),
            'total_data': int(value[3])
        })

    if end_date is not None and start_date is not None:
        response['interval'] = f'{getIntervalDate(start_date, end_date)} Days'

    response['data'] = data

    return jsonify(response)


@app.route('/average')
def getAverageOne():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')
        category = args.get('category')

        data = []
        response = {}

        if end_date is None or start_date is None:
            return jsonify({
                'message': 'start date & end date required'
            })

        interval = getIntervalDate(start_date, end_date)

        query = f'''
        WITH tmp as(SELECT 
            COALESCE((
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float
                - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))::float
            ) 
            / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
            * 100 AS percentage_change,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
            category
            FROM feedback group by category)
            
            SELECT avg(percentage_change) from tmp
        '''

        if category is not None:
            query = f'''
            WITH tmp as(SELECT 
                COALESCE((
                    (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float
                    - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))::float
                ) 
                / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
                * 100 AS percentage_change,
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float as current_score,
                SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
                category
                FROM feedback
                WHERE category = '{category}'
                group by category
                )
                
                SELECT avg(percentage_change) from tmp
        '''

        results = db.session.execute(text(query))

        for value in results:
            data.append({
                'average': float(value[0]),
                'category': category
            })

        response['data'] = data[0]

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500


@app.route('/average-list')
def getAverageList():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')
        category = args.get('category')

        data = []
        response = {}

        if end_date is None or start_date is None:
            return jsonify({
                'message': 'start date & end date required'
            })

        interval = getIntervalDate(start_date, end_date)

        query = f'''
        WITH tmp as(SELECT 
            COALESCE((
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float
                - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))::float
            ) 
            / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
            * 100 AS percentage_change,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
            category
            FROM feedback group by category)
            
            select avg(percentage_change), sum(current_score) as current, sum(before_score) as before, category from tmp group by category
        '''

        if category is not None:
            query = f'''
            WITH tmp as(SELECT 
                COALESCE((
                    (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float
                    - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))::float
                ) 
                / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
                * 100 AS percentage_change,
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float as current_score,
                SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
                category
                FROM feedback
                WHERE category = '{category}'
                group by category
                )
                
                select avg(percentage_change), sum(current_score) as current, sum(before_score) as before, category from tmp group by category
        '''

        results = db.session.execute(text(query))

        for value in results:
            data.append({
                'average': float(value[0]),
                'current': value[1],
                'before': value[2],
                'category': value[3],
            })

        response['data'] = data

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500


@app.route('/diff')
def getDiffPercentageDataOne():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')

        data = []
        response = {}

        if end_date is None and start_date is None:
            return jsonify({
                'message': 'start date & end date required'
            })

        interval = getIntervalDate(start_date, end_date)

        query = f'''
        SELECT 
            COALESCE((
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))
                - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))
            ) 
            / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
            * 100 AS percentage_change,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}')) as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score
        FROM feedback;
        '''

        results = db.session.execute(text(query))

        if end_date is not None and start_date is not None:
            response['interval'] = f'{getIntervalDate(start_date, end_date)} Days'

        for value in results:
            data.append({
                'percentage': float(value[0]),
                'current': float(value[1]),
                'before': float(value[2]),
            })

        response['data'] = data[0]

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500


@app.route('/diff-list')
def getDiffPercentageDataList():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')

        data = []
        response = {}

        if end_date is None or start_date is None:
            return jsonify({
                'message': 'start date & end date required'
            })

        interval = getIntervalDate(start_date, end_date)

        query = f'''
        SELECT 
            COALESCE((
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float
                - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))::float
            ) 
            / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
            * 100 AS percentage_change,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))::float as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
            category
        FROM feedback group by category;
        '''

        results = db.session.execute(text(query))

        if end_date is not None and start_date is not None:
            response['interval'] = f'{interval} Days'

        for value in results:
            data.append({
                'percentage': float(value[0]),
                'current': value[1],
                'before': value[2],
                'category': value[3],
            })

        response['data'] = data

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500


@app.route('/series')
def getSeriesOne():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')
        category = args.get('category')

        data = []
        response = {}

        if end_date is None or start_date is None:
            return jsonify({
                'message': 'start date, end date required'
            })

        interval = getIntervalDate(start_date, end_date)

        query = f'''
        SELECT 
            COALESCE((
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))
                - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))
            ) 
            / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
            * 100 AS percentage_change,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}')) as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score
        FROM feedback
        '''

        if category is not None:
            query += f" where category = '{category}'"
        results = db.session.execute(text(query))

        if end_date is not None and start_date is not None:
            response['interval'] = f'{interval} Days'

        for value in results:
            data.append({
                'percentage': float(value[0]),
                'current_score': int(value[1]),
                'last_score': int(value[2]),
                'category': category
            })

        response['data'] = data[0]

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500


@app.route('/series-list')
def getListSeries():
    try:
        args = request.args
        start_date = args.get('start_date')
        end_date = args.get('end_date')
        category = args.get('category')

        series = []
        data = []
        response = {}

        if end_date is None or start_date is None or category is None:
            return jsonify({
                'message': 'start date, end date, category required'
            })

        interval = getIntervalDate(start_date, end_date)

        query = f'''
        SELECT 
            COALESCE((
                (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}'))
                - (SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days'))
            ) 
            / SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float, 0)
            * 100 AS percentage_change,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}')) as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
            category
        FROM feedback group by category
        '''

        queryGetDataSeries = f'''
        SELECT 
            date_trunc('day', date) as date,
            (SUM(score) FILTER (WHERE date >= '{start_date}' AND date < '{end_date}')) as current_score,
            SUM(score) FILTER (WHERE date >= to_date('{start_date}', 'yyyy-mm-dd') - interval '{interval} days' AND date < to_date('{end_date}', 'yyyy-mm-dd') - interval '{interval} days')::float as before_score,
            category
        FROM feedback group by category, date_trunc('day', date)
        '''

        results = db.session.execute(text(query))
        resultsDataSeries = db.session.execute(text(queryGetDataSeries))

        response['interval'] = f'{getIntervalDate(start_date, end_date)} Days'

        for value in resultsDataSeries:
            series.append({
                'date': str(value[0]),
                'current_score': value[1],
                'last_score': value[2],
                'category': value[3]
            })

        for value in results:
            data.append({
                'percentage': float(value[0]),
                'current_score': int(value[1]),
                'last_score': int(value[2]),
                'category': value[3],
                'series': list(filter(lambda x: x['category'] == value[3], series))
            })

        response['data'] = data

        return jsonify(response)
    except Exception as err:
        return jsonify({'message': str(err)}), 500

    # data_sorted = sorted(response, key=itemgetter('category'))
    # grouped_data = {}

    # for key, group in groupby(data_sorted, key=itemgetter('category')):
    #     grouped_data[key] = list(group)
    # return jsonify({
    #     'data': grouped_data
    # })


if __name__ == '__main__':
    app.run(debug=True)
